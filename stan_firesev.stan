//
// This Stan program defines a simple model, with a
// vector of values 'y' modeled as normally distributed
// with mean 'mu' and standard deviation 'sigma'.
//
// Learn more about model development with Stan at:
//
//    http://mc-stan.org/users/interfaces/rstan.html
//    https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started
//

// The input data is a vector 'y' of length 'N'.
data {
  int<lower=1> N;
  vector[N] FIRE_LOSS;
  int<lower=1> K; // if we declare as multidimensional
  matrix[N, K] X; 
  vector[N] FIRE_CNT; // calling out FIRE_CNT separate as Offset
  int<lower=1> Z; //this is for ppcheck on a test data set
  // vector[N] CoverageC_Trended_CP_G20_LN;
  // vector[N] Deductible_GP_ln;
  // vector[N] BPR2_G20;
  // vector[N] PY2011;
  // vector[N] PY2012;
  // vector[N] PY2013;
  // vector[N] PY2014;
  // vector[N] PY2015;
  // vector[N] PY2016;
  // vector[N] NewBusinessIndicator;
  // 

}

// Transform data in any way necessary
// Also need to define constants in this block
transformed data {
  
  
}

// The parameters accepted by the model. Our model
// accepts two parameters 'mu' and 'sigma'.
parameters {
  real<lower=0> alpha;
  real intercept;
  vector[K] coef;
}

// I want to transform my mu and phi like a glm
transformed parameters {
vector[N] mu;
vector[N] beta;

mu = exp(intercept + X * coef + FIRE_CNT); // log-link is this
beta = alpha ./ mu;

}

// The model to be estimated. We model the output
// 'y' to be normally distributed with mean 'mu'
// and standard deviation 'sigma'.
model {
  // This is where you typically define priors, but 
  // if you do not, Stan will just assume you
  // wanted vague uniform(-inf, inf) parameters, which most similar
  // to what will come from a MLE based GLM

  // This is our family sampling statement
  FIRE_LOSS ~ gamma(alpha, beta);
}

// we can use generated quantities to come up with
// sample draws from the posterior severity and extract likelihood
generated quantities {
  
  vector[N] trainsample;
  vector[Z] testsample;
  vector[N] log_likelihood;
  real total_log_likelihood;
  
  // grab a simulated set of data
  for(n in 1:N) trainsample[n] = gamma_rng(alpha, beta[n]); 
  for(n in 1:Z) testsample[n] = gamma_rng(alpha, beta[n]);
  
  // get log-likelihood for MLE comparison
  for(n in 1:N) log_likelihood[n] = gamma_lpdf(FIRE_LOSS[n] | alpha,  beta[n]);
    
  total_log_likelihood = sum(log_likelihood);

}
