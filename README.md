# Bayesian Modelling

Introductory presentation to Bayesian analysis with emphasis on Linear Regression models


PowerPoints
- Bayesian Modelling Part I: ~1 hour over view of Bayesian thinking and analysis
	- Bayes Theorem
	- Probability Distirbutions
	- Toy Analysis
	- Priors
	- Monte Carlo algorithms

- Bayesian Modelling Part II: ~1 hour over view of building "real" Bayesian Linear Regression Models
	- Gamma GLM Assumptions
	- Stan programming
	- Bayesian Severity GLM (in R)
	- Severity Model with Coverage Modifications (censorship & truncation)
	- Hierarchical Severity Model

- Bayesian Modelling Express: Parts I and II combined into a 30 minute high speed run through


Scripts
- bayesian_glm_example.R: Examples for Bayesian Modelling Part II.
	- stan_firesev.stan: stan program created manually to mimic standard GLM
	- brm_stan_firesev.stan: brm produced program that does the same stan_firesev.stan
	- brm_stan_firesev_censtrunc.stan: brm produced program that includes limt and deductible impact to gamma dist
	- brm_stan_firesev_state.stan: brm produced program that inlcudes state hierarchical approach

- bayesian_glm_example_express.R: Reduced Example code for Bayesian Modelling Express
- presentation_data.R: Script to produce plots in Bayesian Modelling Express